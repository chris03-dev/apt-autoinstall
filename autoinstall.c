#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	system("apt --dry-run autoremove > autolist.txt");
	
	FILE *f = fopen("autolist.txt", "r+");
	char buf[256];
	
	while (fgets(buf, 256, f) != NULL)
	{
		if (buf[0] == ' ')
		{
			printf("Line: %s", buf);
			char *pkg = strtok(buf, " ");
			while (pkg != NULL)
			{
				char cmd[268];
				strcpy(cmd, "apt install -qq ");
				strcat(cmd, pkg);
				system(cmd);
				
				pkg = strtok(NULL, " ");
			}
		}
	}
	
	fclose(f);
	remove(f);
	
	return 0;
}